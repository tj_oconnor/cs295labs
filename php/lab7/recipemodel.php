<?php
require_once("model.php");
require_once("dbConn.php");
class Recipe{
	protected $id;
	protected $title;
	protected $instructions;
	protected $ingredients = array();
	function __construct($id, $title, $inst, $ings){
		$this->setID($id);
		$this->setTitle($title);
		$this->setInst($inst);
		$this->setIngs($ings);
	}
	private function setID($id){
		$this->id = $id;
	}	
	private function setTitle($title){
		$this->title = $title;
	}
	private function setInst($inst){
		$this->instructions = $inst;
	}
	private function setIngs($ings){
		$this->ingredients = $ings;
	}
	function getID(){
		return $this->id;
	}
	function getTitle(){
		return $this->title;
	}
	function getInst(){
		return $this->instructions;
	}
	function getIngs(){
		return $this->ingredients;
	}
}

class RecipeModel extends Model{

	function findAll(){
		return qDatabase('getAll');
	}
	function saveRecipe(){
		return qDatabase('saveInput');
	}
	function deleteRecipe(){
		return qDatabase('deleteItem');
	}
	function editRecipe(){
		return qDatabase('editItem');
	}
}