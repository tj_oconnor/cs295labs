<?php
class Database{
	private static $conn = NULL;
	static function getConnection(){
		if(self::$conn == NULL){
			if (isset($_ENV['OPENSHIFT_APP_NAME'])) {
				define("DB_NAME", "CS295Labs");
				define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
				define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
				define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);    
			} else {
				define("DB_NAME", "cs295labs");
				define("DB_HOST", "localhost");
				define("DB_USER", "root");
				define("DB_PASS", "");  
			}

			self::$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if(mysqli_connect_errno()){
				die("This is not the connection you're looking for!\nError Number".mysqli_connect_errno());
			}
		}
		return self::$conn;
	}
}
function qDatabase($q){
	$conn = Database::getConnection();
	return $q($conn);
}
function getAll($conn){
		$recipeBook = array();
		$res = $conn->query("SELECT * FROM recipes");
		while($row = $res->fetch_assoc()){
			$ingRes = $conn->query("SELECT * FROM ingredients AS ing
                                    JOIN rec_ing AS rec ON rec.ingID=ing.ingID
									WHERE rec.recID=".$row{'recID'});
			$ings = array();
				while($ing = $ingRes->fetch_assoc()){
					$ings[] = $ing{'ingName'};
				}
		
			$recipeBook[] =  new Recipe($row{'recID'}, $row{'title'}, $row{'directions'}, $ings);
		}
		$res->free();
		return $recipeBook;
}
function saveInput($conn){
		$ingreds = array();
		$ingIDs = array();
		foreach($_POST['ingredient'] as $ingred){
			$ingreds[] = htmlentities($ingred);
			$res = $conn->query("SELECT ingID, ingName FROM ingredients
								 WHERE ingName='".$ingred."'");
			$row = $res->fetch_assoc();
			if(!$row){
				$query = $conn->prepare("INSERT INTO ingredients (ingName) VALUES(?)");
				$temp = htmlentities($ingred);
				$query->bind_param('s', $temp);
				$query->execute();
				$ingIDs[] = $conn->insert_id;
				$query->free_result();
			}else{
				$ingIDs[] = $row{'ingID'};
				$res->free();
			}
		}
		$query = $conn->prepare("INSERT INTO recipes (title, directions) VALUES(?,?)");
		$title = htmlentities($_POST['title']);
		$inst = htmlentities($_POST['instructions']);
		$query->bind_param('ss', $title, $inst);
		$query->execute();
		$recID = $conn->insert_id;
		$query->free_result();
		foreach($ingIDs as $ingID){
			$query = $conn->prepare("INSERT INTO rec_ing (recID, ingID) VALUES(?,?)");
			$query->bind_param('ii', $recID, $ingID);
			$query->execute();
			$query->free_result();
		}
		
		return new Recipe($recID, $title, $inst, $ingreds);
}
function deleteItem($conn){
	if(isset($_POST['delID'])){
		foreach($_POST['delID'] as $item){
			$res = $conn->query("DELETE FROM recipes
								 WHERE recID='".$item."'");
		}
	}
	return getAll($conn);
}
function editItem($conn){

}

