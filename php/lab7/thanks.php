<h2>Thank You For Your Submission <em></em></h2>	        
<fieldset>
	<legend>Recipe Card</legend>
	<hr />
	<div id="titleDiv"><?php echo htmlentities($this->data->getTitle()); ?></div>
	<div id="cardBodyDiv">
		<div id="ingDiv">
			<?php foreach($this->data->getIngs() as $ing){ 
				echo htmlentities($ing);?>
				<hr />
			<?php } ?>
		</div>
		<div id="instructDiv">
			<?php echo htmlentities($this->data->getInst()); ?>
		</div>
	</div>
	<a href="index.php">Return to recipe list</a>				
</fieldset>