<a href="index.php?action=insert">Insert a recipe</a>
<p>

<form action="index.php?action=delete" method="post">
	<table id="recipeTable">
		 <thead>
			<tr>
				<th> </th>
				<th>Title</th>                  
				<th>Ingredients</th>
				<th>Instructions</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($this->data as $recipe) { ?>     
			<tr>
				<td><input type="checkbox" name="delID[]" 
					value=<?php echo htmlentities($recipe->getID()); ?>></td>
				<td><?php echo htmlentities($recipe->getTitle()); ?></td>                                
				<td>
					<ul><?php
						foreach($recipe->getIngs() as $ing){ ?>
						<li><?php echo htmlentities($ing); ?></li>
						<?php } ?>
					</ul>
				</td>
				<td><?php echo htmlentities($recipe->getInst()); ?></td>
			</tr>                                
		<?php } ?>          
		</tbody>        		
	</table>
	<button type="submit" class="submitBTN" name="delete">Delete</button>	
</form>
