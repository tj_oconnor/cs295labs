<h2>Thank You For Your Submission <em></em></h2>	        
<fieldset>
	<legend>Recipe Card</legend>
	<hr />
	<div id="titleDiv"><?php echo htmlentities($this->data->title); ?></div>
	<div id="cardBodyDiv">
		<div id="ingDiv">
			<?php
				echo htmlentities($this->data->ingredient0);
				echo '<hr />';
				echo htmlentities($this->data->ingredient1);
				echo '<hr />';
				echo htmlentities($this->data->ingredient2);
				echo '<hr />';
			?>
		</div>
		<div id="instructDiv">
			<?php echo htmlentities($this->data->instructions); ?>
		</div>
	</div>
	<a href="index.php">Return to recipe list</a>				
</fieldset>