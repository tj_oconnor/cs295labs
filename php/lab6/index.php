<?php
require_once("recipemodel.php");
require_once("view.php");
$view = NULL;
$recipeModel = new RecipeModel();
if (isset($_GET['action']) && $_GET['action'] == "insert"){
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		$view = new View('form', NULL);
	}else{
		$view = new View('thanks', $recipeModel->saveRecipe());
	}
}else{
	$view = new View('list', $recipeModel->findAll());
}
	$view->render();
