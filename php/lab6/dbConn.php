<?php
class Database{
	private static $conn = NULL;
	static function getConnection(){
		if(self::$conn == NULL){
			if (isset($_ENV['OPENSHIFT_APP_NAME'])) {
				define("DB_NAME", "CS295Labs");
				define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
				define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
				define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);    
			} else {
				define("DB_NAME", "cs295labs");
				define("DB_HOST", "localhost");
				define("DB_USER", "root");
				define("DB_PASS", "");  
			}

			self::$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if(mysqli_connect_errno()){
				die("This is not the connection you're looking for!\nError Number".mysqli_connect_errno());
			}
		}
		return self::$conn;
	}
}
function qDatabase($q){
	$conn = Database::getConnection();
	return $q($conn);
}
function getAll($conn){
		$recipeBook = array();
		$res = $conn->query("SELECT * from recipe");
		while($row = $res->fetch_assoc()){
			$recipeBook[] =  new Recipe($row{'id'}, $row{'title'}, $row{'ingredient0'}, $row{'ingredient1'}, 
								$row{'ingredient2'}, $row{'directions'});
		}
		$res->free();
		return $recipeBook;
}
function saveInput($conn){
		$query = $conn->prepare("INSERT INTO recipe (title, ingredient0, ingredient1, ingredient2, directions) VALUES(?,?,?,?,?)");
		$ingreds = array();
		foreach($_POST['ingredient'] as $ingred){
			$ingreds[] = htmlentities($ingred);
		}
		$title = htmlentities($_POST['title']);
		$inst = htmlentities($_POST['instructions']);
		$query->bind_param('sssss', $title, $ingreds[0], $ingreds[1], $ingreds[2], $inst);
		$query->execute();
		$query->free_result();
		return new Recipe(NULL, $title, $ingreds[0], $ingreds[1], $ingreds[2], $inst);
}

