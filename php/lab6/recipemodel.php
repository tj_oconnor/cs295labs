<?php
require_once("model.php");
require_once("dbConn.php");
class Recipe{
	public $id;
	public $title;
	public $ingredient0;
	public $ingredient1;
	public $ingredient2;
	public $instructions;
	function __construct($id, $title, $ing0,$ing1, $ing2, $inst){
		$this->id = $id;
		$this->title = $title;
		$this->ingredient0 = $ing0;
		$this->ingredient1 = $ing1;
		$this->ingredient2 = $ing2;
		$this->instructions = $inst;
	}
}
class RecipeModel extends Model{

	function findAll(){
		return qDatabase('getAll');
	}
	function saveRecipe(){
		return qDatabase('saveInput');
	}
}