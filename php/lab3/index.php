<?php
include "../phpFuncs.php";
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        
        <link href="../css/default.css" rel="stylesheet">
	</head>
    <body> 
		<?php
			if ($_SERVER['REQUEST_METHOD'] == "GET"){
					disInput();
				}elseif ($_SERVER['REQUEST_METHOD'] == "POST"){
					disOutput();
				}else{
					header("Location: ../error.php");
				}
		?>
        
    </body>
</html>
