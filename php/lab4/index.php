<?php

function render($page){
	ob_start();
	include_once '../phpFuncs.php';
	include_once $page.'.php';
	$content = ob_get_clean();
	include_once 'layout.php';
}

if (isset($_GET['action']) && $_GET['action'] == "insert"){
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		render("form");
	}else{
		render("thanks");
	}
}else{
	render("list");
}

