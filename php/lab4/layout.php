<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab4</title>
        
        <link href="../css/default.css" rel="stylesheet">
	</head>
    <body> 
		<div id="container">
			<h1 class="title">Recipe Database</h1>
			<?php echo $content; ?>
		</div>    
    </body>
</html>