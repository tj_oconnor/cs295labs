<?php
class Database{
	private static $conn = NULL;
	static function getConnection(){
		if(self::$conn == NULL){
			if (isset($_ENV['OPENSHIFT_APP_NAME'])) {
				define("DB_NAME", "CS295Labs");
				define("DB_HOST", $_ENV['OPENSHIFT_MYSQL_DB_HOST']);
				define("DB_USER", $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);
				define("DB_PASS", $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);    
			} else {
				define("DB_NAME", "cs295labs");
				define("DB_HOST", "localhost");
				define("DB_USER", "root");
				define("DB_PASS", "");  
			}

			self::$conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
			if(mysqli_connect_errno()){
				die("This is not the connection you're looking for!\nError Number".mysqli_connect_errno());
			}
		}
		return self::$conn;
	}
}
