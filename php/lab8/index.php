<?php
require_once("recipemodel.php");
require_once("view.php");
$view = NULL;
session_start();
if(isset($_SESSION['username'])){
	$recipeModel = new RecipeModel();
	if (isset($_GET['action']) && $_GET['action'] == "insert"){
		if($_SERVER['REQUEST_METHOD'] == "GET"){
			$view = new View('head','form', NULL);
		}else{
			$view = new View('head','thanks', $recipeModel->saveRecipe());
		}
	}elseif (isset($_GET['action']) && $_GET['action'] == "delete"){
		$view = new View('head','list', $recipeModel->deleteRecipe());
	}else{
		$view = new View('head','list', $recipeModel->findAll());
	}
	$view->render();

}else{
	header('Location: login.php');
}