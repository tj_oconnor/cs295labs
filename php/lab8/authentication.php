<?php
require_once 'dbConn.php';

function hash_password($password) {
    $cost = '10';
    $salt = '3xt3rm1nat3';
    return crypt($password, '$2a$' . $cost . '$' . $salt . '$');
}
function success(){
	$user = htmlentities($_POST['user']);
	$pass = hash_password(htmlentities($_POST['pass']));
	$conn = Database::getConnection();
	$res = $conn->query("SELECT userName, userPW FROM usertable
						 WHERE userName='".$user."'");
	$row = $res->fetch_assoc();
	$res->free();
	if($row{'userPW'}==$pass){
		session_start();
		session_regenerate_id();
		$_SESSION['username'] = $user;
		return true;
	}
	return false;
}
function logoff(){

	session_start();
	$_SESSION = array();
	if(session_id() != "" || isset($_COOKIE[session_name()])){
		setcookie(session_name(),'', time()-2592000, '/');
		session_unset();
		session_destroy();
	}
}