<?php
require_once("authentication.php");
require_once("view.php");
$view = NULL;
if (isset($_GET['action']) && $_GET['action'] == "login"){
	if(success()){
		header('Location: index.php');
	}else{
		$view = new View(NULL, 'loginForm', "THESE ARE NOT THE CREDENTIALS YOU'RE LOOKING FOR");
	}
}else{ 
	if(isset($_GET['action']) && $_GET['action'] == "logoff"){
		logoff();
	}
	$view = new View(NULL, 'loginForm', NULL);
}
	$view->render();
