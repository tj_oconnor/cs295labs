<form action="index.php?action=insert" method="post">
	<fieldset>
		<legend>Recipe Card</legend>
			<hr />
			<div id="titleDiv">
				<input class="title" type="text" placeholder="Title" name="title">
			</div>
			<div id="cardBodyDiv">
				<div id="ingDiv">
					<div id="ing">
						<input class="ingredient" id="ingredient" type="text" placeholder="Ingredient" name="ingredient[]">
						<hr />
					</div>
					<input type="button" value="Add Ingredient" onclick="addInputBX();">
				</div>
				<div id="instructDiv">
					<textarea id="instructions" name="instructions" class="instructions" placeholder="Instructions"></textarea>
				</div>
			</div>
		<button type="submit" class="submitBTN" name="submit">Submit</button>	
		<a href="index.php">Return to recipe list</a>
	</fieldset>
</form>