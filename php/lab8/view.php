<?php
class View{
	private $data;
	private $page;
	private $head;
	function __construct($head, $page, $data){
		$this->data = $data;
		$this->head = $head;
		$this->page = $page;
	}
	function render(){
		ob_start();
		if($this->head != NULL){
			require_once $this->head.'.php';
			$header = ob_get_clean();
		}
		require_once $this->page.'.php';
		$content = ob_get_clean();
		require_once 'layout.php';
	}
}