<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab8</title>
        <script src="lib/jsFuncs.js"></script>
		<script src="lib/jquery-1.9.1.js"></script>
        <link href="../css/default.css" rel="stylesheet">
	</head>
    <body> 
	<div id="header">
		<?php if(isset($header)) echo $header;?>
	</div>
		<div id="container">
			<h1 class="title">Recipe Database</h1>
			<?php echo $content; ?>
		</div>    
    </body>
</html>