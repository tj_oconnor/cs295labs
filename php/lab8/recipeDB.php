<?php
require_once 'dbConn.php';

function qDatabase($q){
	$conn = Database::getConnection();
	return $q($conn);
}
function getAll($conn){
		$recipeBook = array();
		$res = $conn->query("SELECT * FROM recipes");
		while($row = $res->fetch_assoc()){
			$ingRes = $conn->query("SELECT * FROM ingredients AS ing
                                    JOIN rec_ing AS rec ON rec.ingID=ing.ingID
									WHERE rec.recID=".$row{'recID'});
			$ings = array();
				while($ing = $ingRes->fetch_assoc()){
					$ings[] = $ing{'ingName'};
				}
		
			$recipeBook[] =  new Recipe($row{'recID'}, $row{'title'}, $row{'directions'}, $ings);
		}
		$res->free();
		return $recipeBook;
}
function saveInput($conn){
		$ingreds = array();
		$ingIDs = array();
		foreach($_POST['ingredient'] as $ingred){
			$ingreds[] = htmlentities($ingred);
			$res = $conn->query("SELECT ingID, ingName FROM ingredients
								 WHERE ingName='".$ingred."'");
			$row = $res->fetch_assoc();
			if(!$row){
				$query = $conn->prepare("INSERT INTO ingredients (ingName) VALUES(?)");
				$temp = htmlentities($ingred);
				$query->bind_param('s', $temp);
				$query->execute();
				$ingIDs[] = $conn->insert_id;
				$query->free_result();
			}else{
				$ingIDs[] = $row{'ingID'};
				$res->free();
			}
		}
		$query = $conn->prepare("INSERT INTO recipes (title, directions) VALUES(?,?)");
		$title = htmlentities($_POST['title']);
		$inst = htmlentities($_POST['instructions']);
		$query->bind_param('ss', $title, $inst);
		$query->execute();
		$recID = $conn->insert_id;
		$query->free_result();
		foreach($ingIDs as $ingID){
			$query = $conn->prepare("INSERT INTO rec_ing (recID, ingID) VALUES(?,?)");
			$query->bind_param('ii', $recID, $ingID);
			$query->execute();
			$query->free_result();
		}
		
		return new Recipe($recID, $title, $inst, $ingreds);
}
function deleteItem($conn){
	if(isset($_POST['delID'])){
		foreach($_POST['delID'] as $item){
			$res = $conn->query("DELETE FROM recipes
								 WHERE recID='".$item."'");
		}
	}
	return getAll($conn);
}
function editItem($conn){

}

