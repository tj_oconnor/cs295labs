<h2>Thank You For Your Submission <em></em></h2>	        
<fieldset>
	<legend>Recipe Card</legend>
	<hr />
	<div id="titleDiv"><?php echo getTitle(); ?></div>
	<div id="cardBodyDiv">
		<div id="ingDiv">
			<?php displayIngr(); ?>
		</div>
		<div id="instructDiv">
			<?php echo getInst(); ?>
		</div>
	</div>
	<a href="index.php">Return to recipe list</a>				
</fieldset>