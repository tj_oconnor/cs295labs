<?php
class View{
	private $data;
	private $page;
	function __construct($page, $data){
		$this->data = $data;
		$this->page = $page;
	}
	function render(){
		ob_start();
		require_once 'dbConn.php';
		require_once $this->page.'.php';
		$content = ob_get_clean();
		require_once 'layout.php';
	}
}