<?php
require_once("recipemodel.php");
require_once("view.php");
$view = NULL;
if (isset($_GET['action']) && $_GET['action'] == "insert"){
	if($_SERVER['REQUEST_METHOD'] == "GET"){
		$view = new View('form', NULL);
	}else{
		$view = new View('thanks', NULL);
	}
}else{
	$recipeModel = new RecipeModel();
	$view = new View('list', $recipeModel->findAll());
}
	$view->render();
